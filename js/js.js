var btn = $('.back-top');
$(window).scroll(function() {
    if ($(window).scrollTop() > 300) {
        btn.addClass('show');
    } else {
        btn.removeClass('show');
    }
    });

    btn.on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({scrollTop:0}, '800');
});


$(window).scroll(function() {
    function trimText(str ,wordCount){
        var strArray = str.split(' ');
        var subArray = strArray.slice(0, wordCount);
        var result = subArray.join(" ");
        return result + '...';
    }

    var str = $('.box-text .title').text();
    var result = trimText(str, 6);
    $('.box-text .title').text(result);
});